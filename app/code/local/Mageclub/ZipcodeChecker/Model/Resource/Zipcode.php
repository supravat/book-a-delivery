<?php
class Mageclub_ZipcodeChecker_Model_Resource_Zipcode extends Mage_Core_Model_Resource_Db_Abstract {
	
	/**
     * Errors in import process
     *
     * @var array
     */
    protected $_importErrors        = array();
    /**
     * Count of imported stores
     *
     * @var int
     */
    protected $_importedRows        = 0;
    
    /**
     * Overwrite existing store(s)
     * 1=yes, 0=no
     * @var int
     */
    protected $_behavior        = 0;
    
    
    protected function  _construct()
    {
        $this->_init('zipcodechecker/zipcode', 'id');
    }
    
    public function import($object) {
		
		
		$this->_behavior = $object->getRequest()->getPost('behavior');
		$importFile = $_FILES['import_pincode_file'];
	
		if (empty($importFile['tmp_name'])) {
			return ;
		}
		$csvFile = $importFile['tmp_name'];
		
		$this->_importErrors        = array();
        $this->_importedRows        = 0;
        $io     = new Varien_Io_File();
        $info   = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');
        // check and skip headers
        $headers = $io->streamReadCsv();
        
        if ($headers === false || count($headers) < 2) {
            $io->streamClose();
            Mage::throwException(Mage::helper('zipcodechecker')->__('Invalid File Format'));
        }
        
        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();
        
        try {
			$rowNumber  = 1;
			$importData = array();
			while (false !== ($csvLine = $io->streamReadCsv())) {
				$rowNumber ++;
				if (empty($csvLine)) {
                    continue;
                }
                
                $row = $this->_getImportRow($csvLine, $rowNumber);
                
                if ($row !== false) {
					$importData[] = $row;
				}
				 
				if (count($importData) <= 5000) {
					$this->_saveImportData($importData);
					$importData = array();
				}
			}
			
			if ($this->_importErrors) {
				$error = Mage::helper('zipcodechecker')->__('File has not been imported. See the following list of errors: <br>%s', implode(" <br>", $this->_importErrors));
				Mage::throwException($error);
			}
			
			if(empty($this->_importErrors)){
                $this->_saveImportData($importData);
            }
            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
			$adapter->rollback();
			$io->streamClose();
			Mage::throwException($e->getMessage());
		} catch (Exception $e) {
			$adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('zipcodechecker')->__('An error occurred while import pincode csv.'));
		}
		$adapter->commit();
		//add success message
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('zipcodechecker')->__($this->_importedRows.' - Rows imported successfully.'));
        return $this;
	}
	
	
	
	
	/**
     * Validate row for import and return stores array or false
     * Error will be add to _importErrors array
     *
     * @param array $row
     * @param int $rowNumber
     * @return array|false
     */
    protected function _getImportRow($row, $rowNumber = 0)
    {

        // validate row
        if (count($row) < 2) {
            $this->_importErrors[] = Mage::helper('zipcodechecker')->__('Invalid stores format in the Row #%s', $rowNumber);
            return false;
        }
        
        // validate Zipcode
        if(empty($row[0])){
            $this->_importErrors[] = Mage::helper('zipcodechecker')->__('Invalid Zipcode "%s" in the Row #%s.', $row[4], $rowNumber);
            return false;
        }
        
        // validate Status
           if($row[1] != 'Yes' && $row[1] != 'No'){
           $this->_importErrors[] = Mage::helper('zipcodechecker')->__('Invalid Status "%s" in the Row #%s.', $row[6], $rowNumber);
            return false;
        }
        $is_use = ($row[1] == 'Yes') ? 1 :(($row[1] == 'No') ? 2 : '');
        
        
        $storeRow = array(
            'zipcode'        => $row[0],
            'status'         => 1,
            'is_use' 		 => (int) $is_use,
            'created_at'     => Mage::getModel('core/date')->timestamp(time()),
            
        );
        
        return $storeRow;       
    }
	
	/**
     * Save import data batch
     *
     * @param array $zipcodes
     * @return 
     */
    protected function _saveImportData(array $zipcodes)
    {
        if (!empty($zipcodes)) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            
            foreach($zipcodes as $zipcode){
				
                $zipcodeId ='';
                $zipcodeModel = Mage::getModel('zipcodechecker/zipcode');
               
                //check is store already exits
                if(!empty($zipcode['zipcode'])) {
                    $zipcodeModel->load($zipcode['zipcode'],'zipcode');
                    
                    if($zipcodeModel->getId()){
                        $zipcodeId = $zipcodeModel->getId();
                    }
                }
				$zipcodeModel->setData($zipcode);
                if(!empty($zipcodeId)) {
                    $zipcodeModel->setId($zipcodeId);
                }
                
                //check for overwrite existing store(s)
                Mage::log($zipcode, true, 'loger.log');
                if($this->_behavior == 1 || empty($zipcodeId)){
                    $transactionSave->addObject($zipcodeModel);
                }
                
                $transactionSave->addObject($zipcodeModel);
                try{
					//$zipcodeModel->save();
				} catch(Exception $e) {
					Mage::throwException(Mage::helper('zipcodechecker')->__('An error occurred while import pincode csv.'));
				}
            }
            $transactionSave->save();
            $this->_importedRows += count($zipcodes);
            
        }
        
        return $this;
    }
    
    public function isZipcode($zipcode) {
		
		$resource = Mage::getSingleton('core/resource');
		$write = $resource->getConnection('core_write');

		$select = $write->select()
			->from(
				$this->getMainTable(),
				['id','zipcode', 'is_use', 'status']
			)->where(
				'zipcode = ?', $zipcode
			)->where(
				'is_use = ?',1
			)->where(
				'status = ?',1
			);
		//var_dump($results->__toString());
		return $results = $write->fetchRow($select);
	}
	
}
?>
