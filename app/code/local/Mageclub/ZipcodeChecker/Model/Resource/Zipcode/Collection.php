<?php
class Mageclub_ZipcodeChecker_Model_Resource_Zipcode_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

        protected function _construct() {
                $this->_init('zipcodechecker/zipcode');
        }
        
        public function addStoreFilter($store, $withAdmin = true){

			if ($store instanceof Mage_Core_Model_Store) {
				$store = array($store->getId());
			}

			if (!is_array($store)) {
				$store = array($store);
			}

			$this->addFilter('store_id', array('in' => $store));

			return $this;
		}
}

