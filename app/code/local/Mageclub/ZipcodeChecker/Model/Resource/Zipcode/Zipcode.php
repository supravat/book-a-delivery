<?php
class Mageclub_ZipcodeChecker_Model_Resource_Zipcode extends Mage_Core_Model_Resource_Db_Abstract {

    protected function  _construct()
    {
        $this->_init('zipcodechecker/zipcode', 'id');
    }
}
