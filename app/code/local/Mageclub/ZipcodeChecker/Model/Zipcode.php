<?php

class Mageclub_ZipcodeChecker_Model_Zipcode extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('zipcodechecker/zipcode');
    }
    
    public function import($object) {
		$resultObj = $this->_getResource()->import($object);
        if ($resultObj) {
            return $resultObj;
        }
	}
	
	public function isZipcode($zipcode) {
		return $this->_getResource()->isZipcode($zipcode);
	} 
}
