<?php

$installer = $this;
$installer->startSetup();

/**
 * Sql script for zipcode table
 */
$table = $installer->getConnection()
        ->newTable($installer->getTable('zipcodechecker/zipcode'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('zipcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false), 'Zipcode')
        ->addColumn('is_use', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array('nullable' => false,'default' => '0'), 'In Use?')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array('nullable' => false,'default' => '0'), 'Status')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array('nullable' => false,'default' => '0'), 'Store id')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Updated At')
        ->setComment('Zipcpde table');

$installer->getConnection()->createTable($table);
$installer->endSetup();
?>
