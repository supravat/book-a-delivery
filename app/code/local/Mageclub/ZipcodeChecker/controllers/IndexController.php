<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

class Mageclub_ZipcodeChecker_IndexController extends Mage_Core_Controller_Front_Action {
	
    public function IndexAction() { 
	  $this->loadLayout();
      $this->renderLayout();  
    }
    
    public function postAction() {
		
		if (!$this->_validateFormKey()) {
            $this->_redirectReferer();
            return;
        }
		$isAjax = Mage::app()->getRequest()->isAjax();
        if ($isAjax) {
            $layout = $this->getLayout();
            $update = $layout->getUpdate();
            $update->load('zipcodechecker_index_result'); //load the layout you defined in layout xml file
            $layout->generateXml();
            $layout->generateBlocks();
            $output = $layout->getOutput();
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('outputHtml' => $output)));
        }
		
		/*
		
		if (!$this->_validateFormKey()) {
            $this->_redirectReferer();
            return;
        }
		$result = false;
        $zipcode = $this->getRequest()->getPost('zipcode');
        
        // (Mage::getSingleton('log/visitor')->getId() || Mage::getSingleton('customer/session')->isLoggedIn())
        if ($zipcode)  {
			$result = Mage::getSingleton('zipcodechecker/zipcode')->isZipcode($zipcode);
			
			Mage::register('data', $result);
		}
		
		if($result) {
			Mage::getSingleton("core/session")->addSuccess($this->__("Delivery service is available"));
		} else {
			Mage::getSingleton("core/session")->addError($this->__("Delivery service isn't available"));
		}
		$bla = Mage::getSingleton('zipcodechecker/zipcode');
		$bla->setMsg("Thank You for your visit!");
		$this->_redirectReferer();*/
	}
}
