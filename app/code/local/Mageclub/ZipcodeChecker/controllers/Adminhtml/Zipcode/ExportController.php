<?php

class Mageclub_ZipcodeChecker_Adminhtml_Zipcode_ExportController extends Mage_Adminhtml_Controller_Action {

    /**
     * Initialize layout.
     *
     * @return Bluehorse_Warehouse_Adminhtml_ExportwarehouseController
     */
    protected function _initAction() {
        $this->_title($this->__('Zipcode Management'))
                ->loadLayout()
                ->_setActiveMenu('zipcodechecker/zipcode');

        return $this;
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('wms/items');
    }

    public function indexAction() {
		
		$this->_initAction()
            ->_title($this->__('Export'))
            ->_addBreadcrumb($this->__('Export'), $this->__('Export'));

        $this->renderLayout();
    }

    /**
     * Get grid-filter of entity attributes action.
     *
     * @return void
     */
    public function getFilterAction()
    {
		
        $data = $this->getRequest()->getParams();
        return;
        if ($this->getRequest()->isXmlHttpRequest() && $data) {
            try {
                //$this->loadLayout();

                /** @var $attrFilterBlock Mage_ImportExport_Block_Adminhtml_Export_Filter */
                //$attrFilterBlock = $this->getLayout()->getBlock('export.filter');
                /** @var $export Mage_ImportExport_Model_Export */
                //$export = Mage::getModel('zipcodechecker/zipcode');			
                
                //return $this->renderLayout();
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('No valid data sent'));
        }
        $this->_redirect('*/*/index');
    }    
}
