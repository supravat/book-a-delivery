<?php

class Mageclub_ZipcodeChecker_Adminhtml_Zipcode_IndexController extends Mage_Adminhtml_Controller_Action {

    /**
     * Initialize layout.
     *
     * @return Bluehorse_Zipcode_Adminhtml_ExportZipcodeController
     */
    protected function _initAction() {
        $this->_title($this->__('Manage Zipcode'))
                ->loadLayout()
                ->_setActiveMenu('zipcodechecker/zipcode');

        return $this;
    }

    /**
     * Check access (in the ACL) for current user
     *
     * @return bool
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('zipcodechecker/zipcodechecker');
    }
    public function indexAction() {
        $this->loadLayout();
        $myblock = $this->getLayout()->createBlock('zipcodechecker/adminhtml_zipcodechecker');
        $this->_addContent($myblock);
        $this->renderLayout();
    }
    public function newAction() {

        $this->_title($this->__("Zipcode"));
        

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("zipcodechecker/zipcode")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("zipcodechecker_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("zipcodechecker/zipcode");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Zipcode Manager"), Mage::helper("adminhtml")->__("Zipcode Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Zipcode Description"), Mage::helper("adminhtml")->__("Zipcode Description"));


        $this->_addContent($this->getLayout()->createBlock("zipcodechecker/adminhtml_zipcodechecker_edit"))
                ->_addLeft($this->getLayout()->createBlock("zipcodechecker/adminhtml_zipcodechecker_edit_tabs"));

        $this->renderLayout();
    }
    
    
    public function saveAction() {

        $post_data = $this->getRequest()->getPost();

        if ($post_data) {

            try {
                
                
                if(isset($post_data['stores'])) {
					if(in_array('0',$post_data['stores'])){
						$post_data['store_id'] = '0';
					}
					else{
						$post_data['store_id'] = implode(",", $post_data['stores']);
					}
				   unset($post_data['stores']);
				}
                $model = Mage::getModel("zipcodechecker/zipcode")
                        ->addData($post_data)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Zipcode was successfully saved"));

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }  
    
    
    public function editAction() {
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("zipcodechecker/zipcode")->load($id);
        if ($model->getId()) {
            Mage::register("zipcodechecker_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("zipcodechecker/zipcode");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Zipcode Manager"), Mage::helper("adminhtml")->__("Zipcode Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Zipcode Description"), Mage::helper("adminhtml")->__("Zipcode Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("zipcodechecker/adminhtml_zipcodechecker_edit"))
				->_addLeft($this->getLayout()->createBlock("zipcodechecker/adminhtml_zipcodechecker_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("zipcodechecker")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("zipcodechecker/Zipcode");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction() {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("zipcodechecker/Zipcode");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }  
	
	public function exportCsvAction()
	{
		$fileName   = 'zipcode.csv';
		$content    = $this->getLayout()->createBlock('zipcodechecker/adminhtml_zipcodechecker_grid')
		->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
		$this->_prepareDownloadResponse($fileName, $content, $contentType);
	}    

}
