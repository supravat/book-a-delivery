<?php
class Mageclub_ZipcodeChecker_Adminhtml_Zipcode_ImportController extends Mage_Adminhtml_Controller_Action {

    /**
     * Initialize layout.
     *
     * @return Mageclub_ZipcodeChecker_Adminhtml_Zipcode_ImportController
     */
    protected function _initAction() {
        $this->_title($this->__('Retails Store Management'))
                ->loadLayout()
                ->_setActiveMenu('zipcodechecker/zipcodechecker');

        return $this;
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('zipcodechecker/zipcodechecker');
    }

    public function indexAction() {
		
		$this->loadLayout();
		$maxUploadSize = Mage::helper('zipcodechecker')->getMaxUploadSize();
		
        $this->_getSession()->addNotice(
                $this->__('Size - Total size of uploadable files must not exceed %s', $maxUploadSize)
        );
        $this->_getSession()->addNotice(
                $this->__('Maximum 5000 rows allowed')
        );
        $this->_getSession()->addNotice(
                $this->__('Sample CSV File Format - First export stores and use exported csv file as sample csv file.')
        );
        $this->_initAction()
				->_setActiveMenu('zipcodechecker/zipcode/index')
                ->_title($this->__('Import CSV file'))
                ->_addBreadcrumb($this->__('Import CSV file'), $this->__('Import CSV file'));
                
		$this->_addContent($this->getLayout()->createBlock('zipcodechecker/adminhtml_import_edit'));
		$this->renderLayout();
    }
	
	/**
     * Import csv file & Save into DB
     */
    public function saveAction() {
        // check if data sent
        
        
        
        if ($data = $this->getRequest()->getPost()) {
            $session = Mage::getSingleton('adminhtml/session');
            $importFile = $_FILES['import_pincode_file'];
            try {
                if ($importFile['error'] == 0) {
                    //check for csv file
                    $fileExtension = pathinfo($importFile['name'], PATHINFO_EXTENSION);
                    if (!empty($fileExtension) && $fileExtension == 'csv') {
                        $resultObj = Mage::getModel('zipcodechecker/zipcode')->import($this);
                    } else {
                        Mage::throwException(Mage::helper('zipcodechecker')->__('Invalid File Format'));
                    }
                } else {
                    Mage::throwException(Mage::helper('zipcodechecker')->__('Error: ' . $importFile['error']));
                }
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            }
            $this->_redirect('*/*/');
        }
    }
}
