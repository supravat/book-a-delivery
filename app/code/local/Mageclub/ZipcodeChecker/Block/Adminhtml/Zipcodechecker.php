<?php

class Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {

        $this->_controller = 'adminhtml_zipcodechecker';
        $this->_blockGroup = 'zipcodechecker';
        $this->_headerText = Mage::helper('zipcodechecker')->__('Manage Zip Code');
        $this->_addButtonLabel = Mage::helper('zipcodechecker')->__('Add New Zip Code');
        parent::__construct();
    }

}
