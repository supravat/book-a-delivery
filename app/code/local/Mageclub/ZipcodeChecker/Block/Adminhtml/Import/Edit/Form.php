<?php
/**
 * Import edit form block
 */
class Mageclub_ZipcodeChecker_Block_Adminhtml_Import_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
		
        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/save'),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('zipcodechecker')->__('Import Pincode')));
        
        $fieldset->addField('behavior', 'select', array(
            'name'     => 'behavior',
            'title'    => Mage::helper('zipcodechecker')->__('Overwrite existing zipcode(s)'),
            'label'    => Mage::helper('zipcodechecker')->__('Overwrite existing zipcode(s)'),
            'required' => true,
            'values'   => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'   => 1,
        ));
        
        $fieldset->addField('import_file', 'file', array(
            'name'     => 'import_pincode_file',
            'label'    => Mage::helper('zipcodechecker')->__('Select File to Import'),
            'title'    => Mage::helper('zipcodechecker')->__('Select File to Import'),
            'required' => true
        ));
        
        

        $form->setUseContainer(true);
        $this->setForm($form);
	
	
	
        return parent::_prepareForm();
    }
}
