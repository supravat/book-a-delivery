<?php

class Mageclub_ZipcodeChecker_Block_Adminhtml_ZipcodeChecker_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('zipcodecheckerGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
		//load collection 
        $collection = Mage::getModel('zipcodechecker/zipcode')->getCollection();
        foreach($collection as $link){
			if($link->getStoreId() && $link->getStoreId() != 0 ){
				$link->setStoreId(explode(',',$link->getStoreId()));
			}
			else{
				$link->setStoreId(array('0'));
			}
		}
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('id', array(
            'header' => Mage::helper('zipcodechecker')->__('ID #'),
            'align' => 'left',
            'index' => 'id',
        ));
		if (!Mage::app()->isSingleStoreMode()) {
			$this->addColumn('store_id', array(
				'header'        => Mage::helper('zipcodechecker')->__('Store View'),
				'index'         => 'store_id',
				'type'          => 'store',
				'store_all'     => true,
				'store_view'    => true,
				'sortable'      => true,
				'filter_condition_callback' => array($this,
					'_filterStoreCondition'),
			));
		}

        $this->addColumn('zipcode', array(
            'header' => Mage::helper('zipcodechecker')->__('Zipcode'),
            'align' => 'left',
            'index' => 'zipcode',
        ));
		
		$this->addColumn('status', array(
            'header' => Mage::helper('zipcodechecker')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Grid::getEnableDisableArray(),
        ));
        
        $this->addColumn('is_use', array(
            'header' => Mage::helper('zipcodechecker')->__('IsUse ?'),
            'index' => 'is_use',
            'type' => 'options',
            'options' => Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Grid::getYesNoArray(),
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('zipcodechecker')->__('Sample CSV'));

        return parent::_prepareColumns();
    }
	
    public function getRowUrl($row) {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_warehouse', array(
            'label' => Mage::helper('zipcodechecker')->__('Remove'),
            'url' => $this->getUrl('*/zipcode_index/massRemove'),
            'confirm' => Mage::helper('zipcodechecker')->__('Are you sure?')
        ));
        return $this;
    }

	static public function getEnableDisableArray() {
        $data_array = array();
        $data_array[1] = Mage::helper('zipcodechecker')->__('Enable');
        $data_array[2] = Mage::helper('zipcodechecker')->__('Disasble');
        return($data_array);
    }

    static public function getValueEnableDisablArray() {
        $data_array = array();
        foreach (Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Grid::getEnableDisableArray() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return($data_array);
    }

    static public function getYesNoArray() {
        $data_array = array();
        $data_array[1] = Mage::helper('zipcodechecker')->__('Yes');
        $data_array[2] = Mage::helper('zipcodechecker')->__('No');
        return($data_array);
    }

    static public function getYesNoValueArray() {
        $data_array = array();
        foreach (Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Grid::getYesNoArray() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return($data_array);
    }
	
	
	public function exportCsvAction()
	{
		$fileName   = 'zipcode.csv';
		$content    = $this->getLayout()->createBlock('zipcodechecker/adminhtml_zipcodechecker_grid')
		->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
		$this->_prepareDownloadResponse($fileName, $content, $contentType);
	}

}
