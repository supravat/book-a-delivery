<?php

class Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('zipcodechecker_form', array('legend' => Mage::helper('zipcodechecker')->__('Item information')));

       
		if (!Mage::app()->isSingleStoreMode()) {
			$fieldset->addField('store_id', 'multiselect', array(
				'name' => 'stores[]',
				'label' => Mage::helper('zipcodechecker')->__('Store View'),
				'title' => Mage::helper('zipcodechecker')->__('Store View'),
				'required' => true,
				'values' => Mage::getSingleton('adminhtml/system_store')
			->getStoreValuesForForm(false, true),
			));
		}
		else {
			$fieldset->addField('store_id', 'hidden', array(
				'name' => 'stores[]',
				'value' => Mage::app()->getStore(true)->getId()
			));
		}
        
        $fieldset->addField('zipcode', 'text', array(
            'label' => Mage::helper('zipcodechecker')->__('Zipcode'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'zipcode',
        ));
        $fieldset->addField('is_use', 'select', array(
            'label' => Mage::helper('zipcodechecker')->__('Is Use'),
            'values' => Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Grid::getYesNoValueArray(),
            'name' => 'is_use',
        ));
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('zipcodechecker')->__('Status'),
            'values' => Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Grid::getValueEnableDisablArray(),
            'name' => 'status',
        ));
       
		if ( Mage::getSingleton('adminhtml/session')->getZipcodecheckerData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getZipcodecheckerData());
			Mage::getSingleton('adminhtml/session')->setZipcodecheckerData(null);
		} elseif ( Mage::registry('zipcodechecker_data') ) {
			$form->setValues(Mage::registry('zipcodechecker_data')->getData());
		}
        return parent::_prepareForm();
    }

}
