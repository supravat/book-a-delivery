<?php

class Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('zipcodechecker_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('zipcodechecker')->__('Zipcode '));
    }

    protected function _beforeToHtml() {
        $this->addTab('zipcodechecker', array(
            'label' => Mage::helper('zipcodechecker')->__('Zipcode Information'),
            'title' => Mage::helper('zipcodechecker')->__('Zipcode Information'),
            'content' => $this->getLayout()->createBlock('zipcodechecker/adminhtml_zipcodechecker_edit_tab_form')->toHtml(),
        ));
        
        return parent::_beforeToHtml();
    }

}
