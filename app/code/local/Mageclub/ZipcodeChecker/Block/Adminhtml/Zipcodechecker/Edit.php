<?php

class Mageclub_ZipcodeChecker_Block_Adminhtml_Zipcodechecker_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "zipcodechecker";
        $this->_controller = "adminhtml_zipcodechecker";
        $this->_updateButton("save", "label", Mage::helper("zipcodechecker")->__("Save"));
        $this->_updateButton("delete", "label", Mage::helper("zipcodechecker")->__("Delete"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("zipcodechecker")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);



        $this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    public function getHeaderText() {
        if (Mage::registry("zipcodechecker_data") && Mage::registry("zipcodechecker_data")->getId()) {

            return Mage::helper("zipcodechecker")->__("Edit  '%s'", $this->htmlEscape(Mage::registry("zipcodechecker_data")->getZipcode()));
        } else {

            return Mage::helper("zipcodechecker")->__("Add New Zipcode");
        }
    }

}
