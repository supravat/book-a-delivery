<?php
/**
 * Import edit form block
 */
class Mageclub_ZipcodeChecker_Block_Adminhtml_Export_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {	
        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action' => $this->getUrl('*/*/getFilter'),
            'method'  => 'post'
        ));
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('zipcodechecker')->__('Exports Zipcode')));
        
        $fieldset->addField('export_type', 'select', array(
            'name'     => 'export_type',
            'title'    => Mage::helper('zipcodechecker')->__('Type'),
            'label'    => Mage::helper('zipcodechecker')->__('Type'),
            'required' => true,
            'values'   => [
							''	=> '-- Please Select --',
							'zipcode' => Mage::helper('zipcodechecker')->__('Zip Code'),
						  ],
            'onchange' => 'editForm.getFilter();',
        ));
        
        $fieldset->addField('export_file_format', 'select', array(
            'name'     => 'export_file_format',
            'title'    => Mage::helper('zipcodechecker')->__('Export File Format'),
            'label'    => Mage::helper('zipcodechecker')->__('Export File Format'),
            'required' => true,
            'values'   => ['csv' => Mage::helper('zipcodechecker')->__('CSV')],
            'value'   => 1,
        ));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
