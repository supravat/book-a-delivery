<?php
/**
 * Import edit block

 */
 
class Mageclub_ZipcodeChecker_Block_Adminhtml_Export_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() { 
        parent::__construct();	
        $this->removeButton('back')
            ->removeButton('reset')
            ->removeButton('save');
    }
    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->_blockGroup = 'zipcodechecker';
        $this->_controller = 'adminhtml_export';
    }
    /**
     * Get header text
     *
     * @return string
     */
    public function getHeaderText() {
        return Mage::helper('zipcodechecker')->__('Export Zipcode');
    }
}
