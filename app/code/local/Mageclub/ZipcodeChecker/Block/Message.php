<?php   
class Mageclub_ZipcodeChecker_Block_Message extends Mage_Core_Block_Template{
	
	protected $html = NULL;
	
	public function getResult()
	{	
		$zipcode = Mage::app()->getRequest()->getPost('zipcode');
		if ($zipcode)  {
			$result = Mage::getSingleton('zipcodechecker/zipcode')->isZipcode($zipcode);
			Mage::getModel('core/cookie')->set('zipcode', $zipcode);
			
		}
		if(!empty($result)) {
			return $this->__("No need to be in for your delivery. Chilled orders are kept in insulated packaging, just add
any special instruction where to leave your package at the checkout");
		}
		return $this->__("Chilled products are packaged in insulated packaging that will keep products at the required temperature for at
least 24 hours ensuring they reach you in perfect condition.");
	}
}
