<?php
class Mageclub_Shipping_Model_Sales_Order extends Mage_Sales_Model_Order{
	
	
	public function getShippingDescription(){
		
		$desc = parent::getShippingDescription();
		$pickupObject = $this->getPickupObject();
		
		if($pickupObject){
			//$desc .= '<br/><b>Delivery Date</b>: '.$pickupObject->getDeliverydate();			
			//$desc .= '<br/>';
			$desc .= ' ( ' . $pickupObject->getDeliverydate() . ' ): ';
		}
		return $desc;
	}
}
