<?php

class Mageclub_Shipping_Model_Carrier_Customrate
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
	/**
     * Carrier's code, as defined in parent class
     *
     * @var string
     */
    protected $_code = 'mageclub_shipping';
    //protected $_isFixed = true;
	
	public function getFormBlock(){
        return 'mageclub_shipping/pickup';
    }
    
	/**
     * Returns available shipping rates for Inchoo Shipping carrier
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        /** @var Mage_Shipping_Model_Rate_Result $result */
        $result = Mage::getModel('shipping/rate_result');
        $postCode = $request->getDestPostcode();
        $expressAvailable = false;
        $orderTotal = 0;
        
        foreach ($request->getAllItems() as $item) {
			$orderTotal += $item->getOriginalPrice() * $item->getQty();
        }
        //Mage::log($orderTotal, null, 'mageclub_shipping.log');
        if($postCode) {
			$cost = $this->_rate($postCode , $orderTotal);
		}
        $result->append($this->_getStandardRate($cost));
        return $result;
    }    
	
	/**
     * Returns Allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return array(
            'standard'    =>  'Standard delivery',
            'express'     =>  'Express delivery',
        );
    }

    /**
     * Get Standard rate object
     *
     * @return Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getStandardRate($cost)
    {
		$title = ($cost)? 'Standard Delivery' : 'Free Delivery';
        /** @var Mage_Shipping_Model_Rate_Result_Method $rate */
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('large');
        $rate->setMethodTitle($title);
        $rate->setPrice($cost);
        $rate->setCost($cost);
        return $rate;
    }

    /**
     * Get Express rate object
     *
     * @return Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getExpressRate()
    {
        /** @var Mage_Shipping_Model_Rate_Result_Method $rate */
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('express');
        $rate->setMethodTitle('Express Delivery');
        $rate->setPrice($this->getConfigData('price'));
        $rate->setCost(0);
        return $rate;
    }
    
    
    protected function _rate($postcode, $subtotal) {
		
		$newHandlingFee 	= 0;
		$isInDelivery 		= false; 

        if (!empty($postcode)) {
			$isInDelivery = Mage::getSingleton('zipcodechecker/zipcode')->isZipcode($postcode);	
		}
		
		if($isInDelivery){
			$newHandlingFee = ($subtotal <= 40) ? 2.95 : 0;
		} else {
			$newHandlingFee = ($subtotal >= 50) ? 4.95 : 7.45;
		}
		return $newHandlingFee;
	}
}
