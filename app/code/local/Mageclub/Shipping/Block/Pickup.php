<?php
class Mageclub_Shipping_Block_Pickup extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
    public function __construct(){
        $this->setTemplate('mageclub/pickup/pickup.phtml');      
    }
}
