<?php

$installer = $this;
$installer->startSetup();

/**
 * Sql script for deliverydate table
 */
$table = $installer->getConnection()
        ->newTable($installer->getTable('deliverydate/deliverydate'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('magento_order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false), 'Magento Order Id')
        ->addColumn('deliverydate', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Created At')
        ->setComment('deliverydate table');
$installer->getConnection()->createTable($table);
$installer->endSetup();
?>
